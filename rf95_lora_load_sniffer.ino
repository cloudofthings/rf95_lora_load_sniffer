#include <Arduino.h>
#include <RH_RF95.h>                // HopeRF RFM96 Library


#define TRANSMITTER_POWER 23        // Valid values 5 to 23 dBm for LoRa transmitter
#define COLLISION_DETECT_TIMEOUT 10000     // Number of ms to wait for CAD
#define RH_FREQUENCY 434.0           // We are at 433MHz. Change the frequency here for other modules.

#define RFM95_CS 8
#define RFM95_RST 4
#define RFM95_INT 3

RH_RF95 driver(RFM95_CS, RFM95_INT);

uint32_t failedCount = 0;
uint32_t successCount = 0;
uint32_t oursCount = 0;

void sniffLoraPacket();

void setup() {
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH); // start HopeRF chip with !RESET pin high
  Serial.begin(115200);
  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);
  if (!driver.setFrequency(RH_FREQUENCY)) {
    Serial.println("failed to set frequency");
  }
  driver.setPromiscuous(true);
  driver.setTxPower(TRANSMITTER_POWER, false);
  driver.setCADTimeout(COLLISION_DETECT_TIMEOUT);
  driver.setThisAddress(120);
  if(!driver.init()){
    Serial.println("i've failed u my loard!");
  }

}



void loop() {
  sniffLoraPacket();
}


void sniffLoraPacket() {
  //sniff lora packet
  if(driver.available()) {
    uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];     // Buffer for incoming and outgoing LoRa messages
    uint8_t len = RH_RF95_MAX_MESSAGE_LEN;
    if(driver.recv(buf, &len)) {
      serial.write(buf, len);
      successCount++;
    } else {
      failedCount++;
    }
  }
}
